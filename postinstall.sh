#!/usr/bin/env bash
#-------------------------------------------------------------------------
#      _          _    __  __      _   _
#     /_\  _ _ __| |_ |  \/  |__ _| |_(_)__
#    / _ \| '_/ _| ' \| |\/| / _` |  _| / _|
#   /_/ \_\_| \__|_||_|_|  |_\__,_|\__|_\__|
#  Arch Linux Post Install Setup and Config
#-------------------------------------------------------------------------

echo -e "\nFINAL SETUP AND CONFIGURATION"

# ------------------------------------------------------------------------

echo -e "\nGenaerating .xinitrc file"

# Generate the .xinitrc file so we can launch Awesome from the
# terminal using the "startx" command
cat <<EOF > ${HOME}/.xinitrc
##!/bin/bash
# Disable bell
xset -b

# Disable all Power Saving Stuff
xset -dpms
xset s off

# X Root window color
xsetroot -solid darkgrey

# Merge resources (optional)
xrdb -merge $HOME/.Xresources

# Caps to Ctrl, no caps
setxkbmap -layout de -option ctrl:nocaps
if [ -d /etc/X11/xinit/xinitrc.d ] ; then
    for f in /etc/X11/xinit/xinitrc.d/?*.sh ; do
        [ -x "\$f" ] && . "\$f"
    done
    unset f
fi

exit 0
EOF

# ------------------------------------------------------------------------

echo -e "\nUpdating /bin/startx to use the correct path"

# By default, startx incorrectly looks for the .serverauth file in our HOME folder.
sudo sed -i 's|xserverauthfile=\$HOME/.serverauth.\$\$|xserverauthfile=\$XAUTHORITY|g' /bin/startx


# ------------------------------------------------------------------------

echo -e "\nDisabling buggy cursor inheritance"

# When you boot with multiple monitors the cursor can look huge. This fixes it.
sudo cat <<EOF > /usr/share/icons/default/index.theme
[Icon Theme]
#Inherits=Theme
EOF

#-------------------------------------------------------------------------

echo -e "\nAdding German KeyboardLayout to System"
#German keyboard Layout

localectl --no-convert set-x11-keymap de pc104 ,dvorak grp:alt_shift_toggle

#echo Section '"InputClass"'          | sudo tee -a /etc/X11/xorg.conf.d/20-keyboard.conf
#echo Identifier '"keyboard"'         | sudo tee -a /etc/X11/xorg.conf.d/20-keyboard.conf
#echo MatchIsKeyboard '"yes"'         | sudo tee -a /etc/X11/xorg.conf.d/20-keyboard.conf
#echo Option '"XkbLayout"' '"de"'     | sudo tee -a /etc/X11/xorg.conf.d/20-keyboard.conf
#echo Option "XkbVariant" '"latin1"'  | sudo tee -a /etc/X11/xorg.conf.d/20-keyboard.conf
#echo EndSection                      | sudo tee -a /etc/X11/xorg.conf.d/20-keyboard.conf

# ------------------------------------------------------------------------

echo -e "\nIncreasing file watcher count"

# This prevents a "too many files" error in Visual Studio Code
echo fs.inotify.max_user_watches=524288 | sudo sudo tee /etc/sysctl.d/40-max-user-watches.conf && sudo sysctl --system

# ------------------------------------------------------------------------

echo -e "\nDisabling Pulse .esd_auth module"

# Pulse audio loads the `esound-protocol` module, which best I can tell is rarely needed.
# That module creates a file called `.esd_auth` in the home directory which I'd prefer to not be there. So...
sudo sed -i 's|load-module module-esound-protocol-unix|#load-module module-esound-protocol-unix|g' /etc/pulse/default.pa

# ------------------------------------------------------------------------

echo -e "\nEnabling Login Display Manager"

sudo systemctl enable lightdm.service

# ------------------------------------------------------------------------

echo -e "\nEnabling xbacklight in Qtile for use with Keybinding and Widget"

touch /etc/X11/xorg.conf.d/20-intel.conf
echo "Section "Device""                               | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo   "Identifier  "Intel Graphics""                 | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo    "Driver      "intel""                         | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo    "Option      "Backlight"  "intel_backlight""  | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf
echo "EndSection"                                     | sudo tee -a /etc/X11/xorg.conf.d/20-intel.conf

sudo touch /etc/udev/rules.d/81-backlight.rules
echo "# Set backlight level to 8"                                                         | sudo tee -a /etc/udev/rules.d/81-backlight.rules
echo "SUBSYSTEM=="backlight", ACTION=="add", KERNEL=="acpi_video0", ATTR{brightness}="8"" | sudo tee -a /etc/udev/rules.d/81-backlight.rules

sudo touch /etc/udev/rules.d/backlight.rules
echo "ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", GROUP="video", MODE="0664"" | sudo tee -a sudo tee -a /etc/udev/rules.d/backlight.rules
# ------------------------------------------------------------------------

echo -e "\n Enabling bluetooth daemon and setting it to auto-start"

sudo sed -i 's|#AutoEnable=false|AutoEnable=true|g' /etc/bluetooth/main.conf
sudo systemctl enable --now bluetooth.service

# ------------------------------------------------------------------------
echo -e "\nEnabling CPU-Frequence controlling"

systemctl enable auto-cpufreq
systemctl enable iwd

#--------------------------------------------------------------------------

echo -e "\n Getting personal Qtile config on to the System"
echo "Please enter desired username: (f.e.: stefan)"
read USER

git clone https://gitlab.com/stefanleis1/data/dotfiles.git /home/${USER}
cp /home/${USER}/dotfiles/.config/qtile/ /home/${USER}/.config/qtile/
# -----------------------------------------------------------------------
echo -e "\nEnabling the cups service daemon so we can print"

systemctl enable --now org.cups.cupsd.service
sudo ntpd -qg
sudo systemctl enable --now ntpd.service
sudo systemctl disable dhcpcd.service
sudo systemctl stop dhcpcd.service
sudo systemctl enable --now NetworkManager.service
echo "
###############################################################################
# Cleaning
###############################################################################
"
# Remove no password sudo rights
sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
# Add sudo rights
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Clean orphans pkg
if [[ ! -n $(pacman -Qdt) ]]; then
	echo "No orphans to remove."
else
	pacman -Rns $(pacman -Qdtq)
fi

# Replace in the same state
cd $pwd
echo "
###############################################################################
# Done
###############################################################################
"
# continue with additional stuff
#bash ~/autoinstall/arch/additional_postinstallscripts/00_allinonescript.sh


