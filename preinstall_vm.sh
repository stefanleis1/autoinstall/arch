
#!/usr/bin/env bash

#-------------------------------------------------------------------------
#      _          _    __  __      _   _
#     /_\  _ _ __| |_ |  \/  |__ _| |_(_)__
#    / _ \| '_/ _| ' \| |\/| / _` |  _| / _|
#   /_/ \_\_| \__|_||_|_|  |_\__,_|\__|_\__|
#  Arch Linux Install Setup and Config
#  Forked of ChrisTitusTech/ArchMatic
#-------------------------------------------------------------------------

echo "This is the configuration script for Arch on a NVME-SSD"
echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download - DE Only"
echo "-------------------------------------------------"
timedatectl set-ntp true
pacman -S --noconfirm pacman-contrib
echo -e "\nInstalling prereqs...\n$HR"
pacman -S --noconfirm gptfdisk btrfs-progs

echo "-------------------------------------------------"
echo "-------select your disk to format----------------"
echo "-------------------------------------------------"
lsblk -f
echo "Please enter disk: (example /dev/sda)"
read DISK
echo "--------------------------------------"
echo -e "\nFormatting disk...\n$HR"
echo "--------------------------------------"

#################### Disk Prep ###########################################################

sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

#################### Create Partitions ###################################################

sgdisk -n 1:0:+500M ${DISK} # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:+500M ${DISK} # partition 2 (Boot),
sgdisk -n 3:0:0     ${DISK} # partition 3 (LVM), with root and home partition later on

#################### Set Partition Types #################################################

sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8300 ${DISK}
sgdisk -t 3:8300 ${DISK}

#################### Label Partitions ####################################################

sgdisk -c 1:"UEFISYS" ${DISK}
sgdisk -c 2:"BOOT"    ${DISK}
sgdisk -c 3:"LVM"     ${DISK}

#################### Make Filesystems ####################################################
echo -e "\nCreating Filesystems...\n$HR"
mkfs.vfat -F32 -n "UEFISYS" ${DISK}1

################### LVM Setup  #############################################################

#################### LUKS encryption ###################################################### 

cryptsetup luksFormat -v --key-size 512 --hash sha256 --iter-time 500 --use-random ${DISK}2
cryptsetup open ${DISK}2  cryptboot 
cryptsetup luksFormat -v --key-size 512 --hash sha256 --iter-time 2000 --use-random ${DISK}3 
cryptsetup open ${DISK}3 lvm 
mkfs.ext4 /dev/mapper/cryptboot 

echo "-----------------------------------------"
echo "------- LVM Partitioning ----------------"
echo "-----------------------------------------"
################### LVM Setup  #############################################################
pvcreate ${DISK}3
vgcreate vg0 ${DISK}3 
lvcreate -L 10G      vg0 -n root 
lvcreate -l 100%FREE vg0 -n home 
mkfs.ext4 /dev/vg0/root 
mkfs.ext4 /dev/vg0/home 
mkfs.ext4  ${DISK}2
################### Mount Target ############################################################

mkdir /mnt/home
mkdir /mnt/boot
mkdir /mnt/boot/EFI
mkdir /mnt/etc
mount /dev/vg0/root /mnt
mount /dev/vg0/home /mnt/home
mount ${DISK}2 /mnt/boot
mount ${DISK}1 /mnt/boot/EFI

################### Generating Fstab-mount Directory #########################################

genfstab -U /mnt >> /mnt/etc/fstab

################### Installing Some Needed Basic Packages For Further Installation ###########

pacstrap /mnt base base-devel linux-lts linux-lts-headers linux-firmware vim nano sudo intel-ucode lvm2 git --noconfirm --needed

##############################################################################################

arch-chroot /mnt
