#!/usr/bin/env bash

## Script to install VMWare + some fixes
yay -S vmware-workstation  --noconfirm --needed

sudo systemctl enable --now vmware-networks.service
sudo systemctl enable --now vmware-usbarbitrator.service
sudo systemctl enable --now vmware-hostd.service
sudo modprobe -a vmw_vmci vmmon
vmware 

#Fix for missing 3D support in VMware
echo "mks.gl.allowBlacklistedDrivers = "TRUE"" | tee -a /home/stefan/.vmware/preferences
sudo modprobe -a vmw_vmci vmmon
