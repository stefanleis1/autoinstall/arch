#!/usr/bin/env bash

#installing all script through a single one
bash ~/autoinstall/arch/additional_postinstallscripts/15_custom_awesome.sh
bash ~/autoinstall/arch/additional_postinstallscripts/10_displaylink.sh
bash ~/autoinstall/arch/additional_postinstallscripts/11_vmware_3D_support.sh
bash ~/autoinstall/arch/additional_postinstallscripts/13_customize_zsh.sh
sudo bash ~/autoinstall/arch/additional_postinstallscripts/14_harden_linux.sh
thunderbird
bash ~/autoinstall/arch/additional_postinstallscripts/17_customized_thunderbird.sh
bash ~/autoinstall/arch/additional_postinstallscripts/18_flatpaks.sh
bash ~/autoinstall/arch/additional_postinstallscripts/12_more_wallpapers.sh

sudo bash /autoinstall/arch/postinstall.sh

