#!/usr/bin/env bash

# installing displayink

yay displaylink --noconfirm --needed

for pkg in $(find /var/cache/pacman/pkg/ -type f -iname "xorg*1.20.8-3*"); do sudo pacman -U $pkg; done

#disabling pageflip
echo 'Section "Device"'          | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo 'Identifier "DisplayLink"'  | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo 'Driver "modesetting"'      | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo 'Option "PageFlip" "false"' | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
echo "EndSection"                | sudo tee -a /usr/share/X11/xorg.conf.d/20-displaylink.conf
