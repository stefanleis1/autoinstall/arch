#!/usr/bin/env bash

echo -e "\nINSTALLING AUR SOFTWARE\n"

cd "${HOME}"

echo "CLOING: YAY"
git clone "https://aur.archlinux.org/yay.git"


PKGS=(

    # UTILITIES -----------------------------------------------------------
   
    'i3lock-fancy'              # Screen locker
    'whatsapp-nativefier'       # Whatsapp
    'autojump'
    'mangohud-git'
    'lib32-mangohud'
    'exa-git'                   # Alternative CLI command for ls
    'nerd-fonts-complete'       # more fonts for system
    'auto-cpufreq-git'          # Manage cpu freq
    'fslint'
    'light-git'
    'timeshift'
    'nerd-fonts-mononoki'
    'nerd-fonts-inconsolata'
    'nerd-fonts-cascadia-code'
    'bpytop'
    # MEDIA ---------------------------------------------------------------

    'lbry-app-bin'              # LBRY Linux Application
    'plex-media-player'         # Plex for Linux
    'shortwave'                 # Radio player app
    # COMMUNICATIONS ------------------------------------------------------
    'whatsapp-nativefier'       # Whatsapp
    'signal'
    'google-calendar-nativefier-dark'
    
    # THEMES --------------------------------------------------------------

    'lightdm-webkit-theme-aether'   # Lightdm Login Theme
    'materia-gtk-theme'             # Desktop Theme
    'papirus-icon-theme'            # Desktop Icons
)


cd ${HOME}/yay
makepkg -si --noconfirm

for PKG in "${PKGS[@]}"; do
    yay -S --noconfirm $PKG
done

echo -e "\nDone!\n"

# Jumping to Post-Installsetup
#sudo bash /autoinstall/arch/postinstall.sh
bash ~/autoinstall/arch/additional_postinstallscripts/00_allinonescript.sh

