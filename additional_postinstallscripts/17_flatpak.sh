#! /usr/bin/env bash

### some flatpak programs

flatpak install flathub com.axosoft.GitKraken -y
flatpak install flathub com.valvesoftware.Steam -y
flatpak install flathub io.github.liberodark.OpenDrive -y
flatpak install flathub io.github.arunsivaramanneo.GPUViewer -y
