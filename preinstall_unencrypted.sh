
#!/usr/bin/env bash

#-------------------------------------------------------------------------
#      _          _    __  __      _   _
#     /_\  _ _ __| |_ |  \/  |__ _| |_(_)__
#    / _ \| '_/ _| ' \| |\/| / _` |  _| / _|
#   /_/ \_\_| \__|_||_|_|  |_\__,_|\__|_\__|
#  Arch Linux Install Setup and Config
#  Forked of ChrisTitusTech/ArchMatic
#-------------------------------------------------------------------------

echo "This is the configuration script for Arch on a NVME-SSD"
echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download - DE Only"
echo "-------------------------------------------------"
timedatectl set-ntp true
pacman -S --noconfirm pacman-contrib
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
curl -s "https://www.archlinux.org/mirrorlist/?country=DE&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist

echo -e "\nInstalling prereqs...\n$HR"
pacman -S --noconfirm gptfdisk btrfs-progs

echo "-------------------------------------------------"
echo "-------select your disk to format----------------"
echo "-------------------------------------------------"
lsblk -f
echo "Please enter disk: (example /dev/sda)"
read DISK
echo "--------------------------------------"
echo -e "\nFormatting disk...\n$HR"
echo "--------------------------------------"

#################### Disk Prep ###########################################################

sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

#################### Create Partitions ###################################################

sgdisk -n 1:0:+500M ${DISK} # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:+500M ${DISK} # partition 2 (Boot),
sgdisk -n 3:0:0     ${DISK} # partition 3 (LVM), with root and home partition later on

#################### Set Partition Types #################################################

sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8300 ${DISK}
sgdisk -t 3:8300 ${DISK}

#################### Label Partitions ####################################################

sgdisk -c 1:"UEFISYS" ${DISK}
sgdisk -c 2:"BOOT"    ${DISK}
sgdisk -c 3:"LVM"     ${DISK}

#################### Make Filesystems ####################################################
echo -e "\nCreating Filesystems...\n$HR"
mkfs.vfat -F32 -n "UEFISYS" ${DISK}1
mkfs.ext4      -n "BOOT"    ${DISK}2

################### LVM Setup  #############################################################

echo "-----------------------------------------"
echo "------- LVM Partitioning ----------------"
echo "-----------------------------------------"

pvcreate ${DISK}3
vgcreate vg0 ${DISK}3 
lvcreate -L 100G vg0 -n root 
lvcreate -l 100%FREE vg0 -n home 
mkfs.ext4 /dev/vg0/root 
mkfs.ext4 /dev/vg0/home 


################### Mount Target ############################################################
mount /dev/vg0/root /mnt
mkdir /mnt/home
mount /dev/vg0/home /mnt/home
mkdir /mnt/boot
mount ${DISK}2 /mnt/boot
mkdir /mnt/boot/EFI
mount ${DISK}1 /mnt/boot/EFI
mkdir /mnt/etc

################### Generating Fstab-mount Directory #########################################

genfstab -U /mnt >> /mnt/etc/fstab

################### Installing Some Needed Basic Packages For Further Installation ###########

pacstrap /mnt base base-devel linux-lts linux-lts-headers linux-firmware vim nano sudo intel-ucode lvm2 git --noconfirm --needed

##############################################################################################

arch-chroot /mnt
