#!/usr/bin/env bash

# fork on manilaromes thunderblurred - https://github.com/manilarome/thunderblurred.git
git clone https://github.com/manilarome/thunderblurred.git
bash -c "$(curl -fsSL https://raw.githubusercontent.com/manilarome/thunderblurred/master/install.sh)"

echo -e "Open Thunderbird and enable 'Dark Mode' in Preferences for it to work"

