#!/usr/bin/env bash
echo "enter User:"
read USER
# Customizing Terminalprompt # 
touch "$HOME/.cache/zshhistory"
#-- Setup Alias in $HOME/zsh/aliasrc
mv /home/${USER}/.zshrc /home/${USER}/.zshrc.latest
cp /home/${USER}/autoinstall/arch/.zshrc /home/${USER}/.zshrc
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc
chsh $USER
/bin/zsh
