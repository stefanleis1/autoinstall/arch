#!/usr/bin/env bash

#### Installation of open-vm-tools with several fixes ####

sudo pacman -S open-vm-tools --noconfirm --needed
sudo systemctl enable --now vmtoolsd
sudo systemctl enable --now vmware-vmblock-fuse
